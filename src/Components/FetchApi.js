import { Component } from "react";

class FetchApi extends Component{
    fetchAPI = async (url , requestOptions) =>{
        

        let response = await fetch(url, requestOptions);
        let data = await response.json();
       
        return data
    }

    getByIdHandler =() =>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = " https://jsonplaceholder.typicode.com/posts/1";
        this.fetchAPI(url , requestOptions)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    
    }

    getAllHandler = () =>{
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        var url = "https://jsonplaceholder.typicode.com/posts";
        this.fetchAPI(url , requestOptions)
            .then((reponse) =>{
                console.log(reponse);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    CreateHandler = () =>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "userId": 1,
            "title": "foo",
            "body": "bar"
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = "https://jsonplaceholder.typicode.com/posts";
        this.fetchAPI(url , requestOptions)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    UpdateHandler =() =>{
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "userId": 1,
            "title": "foo",
            "body": "bar"
        });

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = "https://jsonplaceholder.typicode.com/posts/1";
        this.fetchAPI(url , requestOptions)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }

    DeleteHandler =() =>{
        var requestOptions = {
            method: 'DELETE',
            redirect: 'follow'
        };

        var url = "https://jsonplaceholder.typicode.com/posts/1";
        this.fetchAPI(url , requestOptions)
            .then((response) =>{
                console.log(response);
            })
            .catch((error) =>{
                console.log(error);
            })
    }
    render(){
        return (
            <>
                <div className="row">
                    <div className="col">
                        <button onClick={this.getByIdHandler} className="btn btn-primary">GET BY ID</button>
                    </div>
                    <div className="col">
                        <button onClick={this.getAllHandler} className="btn btn-primary">GET ALL</button>
                    </div>
                    <div className="col">
                        <button onClick={this.CreateHandler} className="btn btn-primary">POST CREATE</button>
                    </div>
                    <div className="col">
                        <button onClick={this.UpdateHandler} className="btn btn-primary">PUT UPDATE</button>
                    </div>
                    <div className="col">
                        <button onClick={this.DeleteHandler} className="btn btn-primary">DELETE</button>
                    </div>
                </div>
            </>
        )
    }
}

export default FetchApi