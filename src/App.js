import "bootstrap/dist/css/bootstrap.min.css";
import FetchApi from "./Components/FetchApi";

function App() {
  return (
    <div className="container mt-5">
        <FetchApi/>
    </div>
  );
}

export default App;
